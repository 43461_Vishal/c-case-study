#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include "library.h"

void member_area(user_t *u){
    int choice;
	char name[80];
    do{
        printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\n5.Issued Books\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:
				printf("Enter book name: ");
				scanf("%s",name);
				book_find_by_name(name);
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				bookcopy_checkavail();
				break;
			case 5: 
				display_issued_bookcopies(u->id);
				break;

		}
	}while (choice != 0);	
}

void bookcopy_checkavail() {
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	printf("enter the book id: ");
	scanf("%d", &book_id);
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}

	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0) {
		
			count++;
		}
	}
	fclose(fp); 
	printf("number of copies availables: %d\n", count);
}
